# Glassfish 3.1.1 build 12

GlassFish is an open-source application server project started by Sun Microsystems for the Java EE platform and now sponsored by Oracle Corporation. The supported version is called Oracle GlassFish Server. GlassFish is free software, dual-licensed under two free software licences: the Common Development and Distribution License (CDDL) and the GNU General Public License (GPL) with the classpath exception.

This repository provides Docker images for GlassFish 3.1.1.2 only. 

## Starting GlassFish Server

```
$ docker pull emersoncoz/glassfish3

```
## Expose ports
Expose ports 80:8080 443:8181 4848:4848, or modify the listeners after starting the container for the desired port.

## Maintenance

This image is currently maintained by the Emerson Cezar. It is used to support specific applications requiring GlassFish 3.1.1 and will not be upgraded to GlassFish 4 or beyond.

## Contact 

Mail: ejco123@gmail.com
Telegram:  https://t.me/emersoncoz
Github:    https://github.com/emersoncoz
Gitlab:    https://gitlab.com/emersoncoz
Dockerhub: https://hub.docker.com/u/emersoncoz