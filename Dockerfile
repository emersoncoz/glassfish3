FROM debian:latest

LABEL Emerson Cezar <emerson-cezar@live.com>

RUN apt-get update && \
    apt-get install -y wget unzip pwgen expect locales locales-all

# Configuring locales 

ENV LANG pt_BR.UTF-8
ENV TZ=America/Sao_Paulo

# Download and configuration Java 

RUN mkdir /usr/java
RUN wget https://files-cdn.liferay.com/mirrors/download.oracle.com/otn-pub/java/jdk/7u80-b15/jdk-7u80-linux-x64.tar.gz
RUN mv jdk-7u80-linux-x64.tar.gz /usr/java/ && \
    tar -zxvf /usr/java/jdk-7u80-linux-x64.tar.gz && \
    mv jdk1.7.0_80 /usr/java/ && \
    rm -rf /usr/java/jdk-7u80-linux-x64.tar.gz
RUN update-alternatives --install /usr/bin/java java /usr/java/jdk1.7.0_80/bin/java 1 
RUN update-alternatives --install /usr/bin/javac javac /usr/java/jdk1.7.0_80/bin/javac 1 

# Download glassfish and configuration

RUN wget http://download.oracle.com/glassfish/3.1.1/release/glassfish-3.1.1.zip
RUN unzip glassfish-3.1.1.zip -d /opt 
RUN rm glassfish-3.1.1.zip

ENV PATH /opt/glassfish3/bin:/opt/app/bin:$PATH

RUN mkdir -p /opt/app/bin
RUN mkdir -p /opt/app/deploy


RUN echo 'root:root' | chpasswd

RUN apt-get clean && rm -rf /var/lib/apt/lists/*

# Download and move mysql-connector-java locale correct    
RUN wget https://downloads.mysql.com/archives/get/p/3/file/mysql-connector-java-5.1.38.zip
RUN unzip mysql-connector-java-5.1.38.zip
RUN cd mysql-connector-java-5.1.38
RUN mv /mysql-connector-java-5.1.38/mysql-connector-java-5.1.38-bin.jar /opt/glassfish3/glassfish/domains/domain1/lib/ext/
RUN rm -rf mysql-connector-java-5.1.38.zip
RUN rm -rf mysql-connector-java-5.1.38 

# Download Glowroot   
RUN wget https://github.com/glowroot/glowroot/releases/download/v0.13.6/glowroot-0.13.6-dist.zip && \
    unzip glowroot-0.13.6-dist.zip && \
    rm glowroot-0.13.6-dist.zip

# Create directory PROPULSOR

RUN mkdir /home/PROPULSOR
RUN mkdir /home/PROPULSOR/logos
RUN chmod -R 777 /home/PROPULSOR/logos

# Workdir

WORKDIR /opt/glassfish3

# 4000 (glowroot), 4848 (admin), 80 (HTTP), 443 (HTTPS)

EXPOSE 80 443 4000 4848 

CMD ["asadmin", "start-domain", "--verbose"]